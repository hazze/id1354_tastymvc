function commentEvent(target){
  target.on('submit', function(e) {
    e.preventDefault();
    var that = $(this),
    url = that.attr('action'),
    type = that.attr('method'),
    data = {};

    // Base url for the site.
    base_url = 'https://hazze.misse.org/tastymvc/';

    // Fetches all the data from the forms parts that has a name attribute and saves them.
    that.find('[name]').each(function(index, value) {
      var that = $(this),
      name = that.attr('name'),
      value = that.val();

      data[name] = value;
    });

    // Send data to the targeted function to handle it.
    $.ajax({
      url: url,
      type: type,
      data: data,
      success: function(response) {
        $(that)[0].reset();
        showComments();
      }
    });
  });
  return false;
};

// Add first event listerner
commentEvent($('form.ajax'));

function showComments(e){
  $.ajax({
    type: 'ajax',
    url: base_url + 'pages/updatecomments',
    async: true,
    dataType: 'json',
    success: function(data){
      var output = '';
      var deletee = '';
      var i;
      for(i = 0; i<data.comments.length;i++){
          if(data.currentuser == data.comments[i].name){
              deletee = '<form action="'+base_url+'pages/delete" method="POST" class="ajax delete_comment">';
              deletee += '<input type="submit" class="button deleteC" name="deleted_comment" value="'+data.comments[i].id+'" /></form>';
          }else{
              deletee = '';
          }
          if(data.comments[i].url == data.currenturl){
            output += '<div class="comment_container">';
            output += '<h3>'+data.comments[i].title+'</h3>';
            output += '<p class="meta">Posted on: '+data.comments[i].date+' by <span>'+data.comments[i].name+'</span></p> ';
            output += deletee;
            output += '<p class="commenttext">'+data.comments[i].commenttext+'</p><br /></div>';
          }
          else {
            console.log("No comment with currenturl");
          }
      }
      // Replace the old content of #comments with the newly fetched comments.
      $('#comments').html(output);
      // Add event listerner to the new comments.
      commentEvent($('#comments form.ajax'));
    },
    error: function(){
      alert('could not get data from database');
    }
  })
  return false;
};
