<?php
  $_SESSION["currenturl"] = $_SERVER["REQUEST_URI"];
  isset($_SESSION["currentuser"]) ? $_SESSION["currentuser"] : '';
?>
<!DOCTYPE html>
<html xml:lang="en" lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>resources/css/style.css?version=<?php time();?>" />

    <title>Tasty Recipes | Home</title>

    <script src="https://code.jquery.com/jquery-1.10.1.min.js"></script>
    <script src="<?php echo base_url();?>/resources/js/functions.js"></script>
    <script src="<?php echo base_url();?>/resources/js/ajaxform.js"></script>
    <?php if (isset($_SESSION["formcomplete"]) ? $_SESSION["formcomplete"] : null) : ?>
      <script>
        bool = <?php echo isset($_SESSION["login"]) ? $_SESSION["login"] : 0; ?>;
        window.onload = atload;
      </script>
    <?php endif;
      if (isset($_SESSION["login"]) ? $_SESSION["login"] : null) : ?>
        <script>
          window.onload = displayLoggedIn;
        </script>
      <?php endif;?>
  </head>
  <body>
    <div id="login" class="loginoverlay">
      <div id="overlaycontent">
        <p id="register" onclick="displayRegForm()" class="button">Sign up</p>
        <p onclick="closeLogin()">&times;</p>
        <br class="clear" />
        <?php if(isset($_SESSION["login"]) ? $_SESSION["login"] : null)
          echo "<img src='". base_url() . "resources/img/avatar_green.png' alt='Silhouette of a person' />";
          else
            echo "<img src='". base_url() . "resources/img/avatar.png' alt='Silhouette of a person' />";
          ?>

        <?php
            $attributes = array('id' => 'loginForm');
            echo form_open('pages/signin', $attributes);
        ?>
          <input
            class="user inputfield"
            type="text"
            name="username"
            placeholder="Username"
            autocomplete="off"
            pattern="[a-zA-Z0-9]+"
            required
          />
          <input
            class="password inputfield"
            type="password"
            name="password"
            placeholder="Password"
            autocomplete="off"
            pattern="[a-zA-Z0-9]+"
            required
          />
          <input
            type="submit"
            class="submit button"
            value="Log In"
            name="submit"
          />
        </form>

        <?php
            $attributes = array('id' => 'regForm');
            echo form_open('pages/register', $attributes);
        ?>
          <input
            class="user inputfield"
            type="text"
            name="username"
            placeholder="Username"
            autocomplete="off"
            pattern="[a-zA-Z0-9]+"
            required
          />
          <input
            class="password inputfield"
            type="password"
            name="password"
            placeholder="Password"
            autocomplete="off"
            pattern="[a-zA-Z0-9]+"
            required
          />
          <input
            class="confirmpassword inputfield"
            type="password"
            name="confirmpassword"
            placeholder="Confirm Password"
            autocomplete="off"
            pattern="[a-zA-Z0-9]+"
            required
          />
          <input type="submit" class="submit button" value="Register" name="submit" />
        </form>
        <div class="response">
          <?php echo (isset($_SESSION["error"]) ? $_SESSION["error"] : null);?>
        </div>
      </div>
    </div>
    <div id="header">
      <div id="nav">
        <div class="wrap">
          <a href="<?php echo base_url(); ?>">
            <img src="<?php echo base_url();?>/resources/img/tasty_logo_small.png" alt="Tasty Recipes Logo Small" id="navLogo" />
          </a>
          <div id="login_nav">
            <?php echo "<p id='user'>" . (isset($_SESSION["currentuser"]) ? $_SESSION["currentuser"] : null) . "&nbsp;</p>";?>
            <input type="button" onclick="openLogin()" id="loginbutton" class="button" value="Log In" />
          </div>
          <ul>
            <li>
              <a href="<?php echo base_url();?>">Home</a>
            </li>
            <li>
              <a href="<?php echo base_url();?>calendar">Calendar</a>
            </li>
            <li>
              <a href="<?php echo base_url();?>#recipes">Recipes</a>
            </li>
            <li>
              <a href="<?php echo base_url();?>about">About</a>
            </li>
          </ul>
          <br class="clear" />
        </div>
        <!-- end nav wrap -->
      </div>
      <!-- end nav -->
