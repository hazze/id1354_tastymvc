<?php
  class Recipe_model extends CI_Model {
    public function __construct() {
      $this->load->database();
    }

    public function get_posts($slug = FALSE) {
      if ($slug === FALSE) {
        $query = $this->db->get('comments');
        return $query->result_array();
      }
      $query = $this->db->get_where('comments', array('url' => $slug));
      return $query->row_array();
    }

    public function create_comment($data) {
      return $this->db->insert('comments', $data);
    }

    public function delete_comment() {
      $target = array(
        'id' => $this->input->post('deleted_comment')
      );
      return $this->db->delete('comments', $target);
    }
  }
