<div id="mainHeader">
  <div class="wrap">
    <img src="<?php echo base_url();?>/resources/img/tasty_logo.png" alt="Tasty Recipes Logo" />
  </div>
  <!-- end mainHeader wrap -->
</div>
<!-- end mainHeader -->
</div>
<!-- end header -->
<div id="recipeContent">
<div class="wrap">
  <div id="recipeText">
    <?php print_xml(lcfirst($title)); ?>
    <h2 id="commenttitle">Comments</h2>
    <div id="comments">
      <?php print_comment($comments, $title); ?>
    </div>
    <?php print_commentform($title); ?>
  </div>
</div>
<!-- end conent wrap -->
</div>
<!-- end content -->
