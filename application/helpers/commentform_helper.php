<?php
/*Start commentform_helper.php file */
function print_commentform($page){
  if (isset($_SESSION["login"]) ? $_SESSION["login"] : null) { ?>
    <div id="commentForm">
      <p id="comment_error">
        <?php echo validation_errors(); ?>
      </p>
      <?php
          $attributes = array('id' => 'comment', 'class' => 'ajax');
          echo form_open('pages/create', $attributes);
      ?>
        <input
          id="title"
          class="inputfield"
          type="text"
          name="title"
          placeholder="Subject"
          autocomplete="off"
          required
        />

        <textarea
          id="text"
          class="inputfield"
          type="text"
          name="text"
          placeholder="Comment text"
          autocomplete="off"
          required></textarea>

        <input
          type="submit"
          id="submit"
          class="button"
          value="Comment"
          name="submit"
        />

        <input
          type="hidden"
          name="url"
          value=<?php echo lcfirst($page).'.php'; ?>
        />

        <input
          type="hidden"
          name="user"
          value=<?php echo $_SESSION['currentuser']; ?>
        />
      <?php echo form_close(); ?>
    <?php
    }
    else
    {
      echo "<p>You need to login to write comments</p>",
           "<a onclick='openLogin()' class='button'>Log in</a>";
    };
}
/*     * End commentform_helper.php file     */
?>
