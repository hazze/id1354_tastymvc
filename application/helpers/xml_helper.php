<?php
/*Start xml_helper.php file */
function print_xml($loadedpage){
    if ($loadedpage == "pancakes")
      $xmlfile = file_get_contents("./resources/xml/pancakes.xml");
    else
      $xmlfile = file_get_contents("./resources/xml/meatballs.xml");

    $cookbook = new SimpleXMLElement($xmlfile);
    echo "<img src='".$cookbook->recipe->imagepath."' alt='Picture of ".$cookbook->recipe->title."' />";
    echo "<h1>".$cookbook->recipe->title,
        "<span>".$cookbook->recipe->quantity." servings</span></h1>";
    echo "<ul id='info'>";
    echo "<li>Prep: ".$cookbook->recipe->preptime."</li>",
         "<li>Cook: ".$cookbook->recipe->cooktime."</li>",
         "<li>Ready in: ".$cookbook->recipe->totaltime."</li></ul>";

    echo "<p id='intro'>",
         $cookbook->recipe->{'description'}->li,
         "</p>";
    echo "<br class='clear' />",
         "<div class='layoutlists'>";
    echo "<h2>Ingredients</h2><ul>";

    foreach ($cookbook->recipe->ingredient->li as $li)
      echo "<li>".$li."</li>", PHP_EOL;

    echo "</div><div class='layoutlists'>",
         "<h2>Instructions</h2><ol>";
    foreach ($cookbook->recipe->recipetext->li as $li)
      echo "<li>".$li."</li>", PHP_EOL;

    echo "</div>";
}
/*     * End xml_helper.php file     */
?>
