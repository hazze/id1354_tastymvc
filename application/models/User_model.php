<?php
  class User_model extends CI_Model {
    public function __construct() {
      $this->load->database();
    }

    public function sign_in($username, $password) {
      $data = array(
        'name' => $username,
        'pass' => $password
      );


      $this->db->where('name', $data['name']);
      $query = $this->db->get('users');
      $query->num_rows();
      if ($query)
      {
        if (password_verify($data['pass'], isset($query->row(0)->pass) ? $query->row(0)->pass : '')) {
          return $query->row(0)->name;
        }
        else
          return FALSE;
      }
      else
        return FALSE;
    }

    public function register_user($username, $password) {
      $password = password_hash($password, PASSWORD_DEFAULT);
      $data = array(
        'name' => $username,
        'pass' => $password
      );


      $this->db->where('name', $data['name']);
      $query = $this->db->get('users');
      if ($query->num_rows() == 0) {
        return $this->db->insert('users', $data);
      }
      else {
        return false;
      }
    }
  }
