<?php
  class Pages extends CI_Controller {
    public function view($page = 'home')
    {
      if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
      {
        show_404();
      }

      $data['title'] = ucfirst($page);

      if($page == 'meatballs' || $page == 'pancakes') {
        $data['comments'] = $this->recipe_model->get_posts();
        $this->load->helper('xml_helper');
        $this->load->helper('comment_helper');
        $this->load->helper('commentform_helper');
      }

      $this->load->view('templates/header', $data);
      $this->load->view('pages/'.$page, $data);
      $this->load->view('templates/footer', $data);
    }

    public function create() {
      $this->form_validation->set_rules('title', 'Title', 'required');
      $this->form_validation->set_rules('text', 'Text', 'required');

      if ($this->form_validation->run() === FALSE) {
        echo "Field required";
      }
      else {
        $data =  array(
          'title' => $this->input->post('title'),
          'commenttext' => $this->input->post('text'),
          'url' => $this->input->post('url'),
          'name' => $this->input->post('user')
        );
        $data['title'] = htmlspecialchars($data['title']);
        $data['commenttext'] = htmlspecialchars($data['commenttext']);

        $this->recipe_model->create_comment($data);
      }
    }

    public function delete() {
      $this->recipe_model->delete_comment();
    }

    public function signin() {
      $this->form_validation->set_rules('username', 'Username', 'required');
      $this->form_validation->set_rules('password', 'Password', 'required');
      if ($this->form_validation->run() === FALSE) {
        echo "Field required";
      }
      else {
        $data = array(
          'name' => $this->input->post('username'),
          'password' => $this->input->post('password')
        );
        $data['name'] = htmlspecialchars($data['name']);
        $data['password'] = htmlspecialchars($data['password']);
        if($this->user_model->sign_in($data['name'], $data['password'])){
          $userdata = array(
            'currentuser' => $data['name'],
            'login' => true
          );
          $this->session->set_userdata($userdata);
          redirect($_SERVER['HTTP_REFERER'], 'refresh');

        }
        else {
          $this->session->set_flashdata('error', 'Wrong username or password.');
          $this->session->set_flashdata('errorreason', 'login');
          $this->session->set_flashdata('formcomplete', true);
          redirect($_SERVER['HTTP_REFERER'], 'refresh');
        };
      };
    }

    public function signout() {
      $this->session->set_flashdata('login', false);
      $this->session->set_flashdata('currentuser', false);
      $this->session->sess_destroy();
      redirect($_SERVER['HTTP_REFERER'], 'refresh');
    }

    public function register() {
      $this->form_validation->set_rules('username', 'Username', 'required');
      $this->form_validation->set_rules('password', 'Password', 'required');
      if ($this->form_validation->run() === FALSE) {
        echo "Field required";
      }
      else {
        $data = array(
          'name' => $this->input->post('username'),
          'password' => $this->input->post('password'),
          'confirm_password' => $this->input->post('confirmpassword')
        );
        $data['name'] = htmlspecialchars($data['name']);
        $data['password'] = htmlspecialchars($data['password']);
        $data['confirm_password'] = htmlspecialchars($data['confirm_password']);

        if (strcmp($data['password'], $data['confirm_password']) == 0) {
          if($this->user_model->register_user($data['name'], $data['password'])) {
            $this->session->set_flashdata('error', 'User registered');
            $this->session->set_flashdata('formcomplete', true);
          }
          else {
            $this->session->set_flashdata('error', 'Username taken');
            $this->session->set_flashdata('formcomplete', True);
            $this->session->set_flashdata('errorreason', 'register');
          };
        }
        else {
          $this->session->set_flashdata('error', 'Passwords does not match.');
          $this->session->set_flashdata('formcomplete', true);
          $this->session->set_flashdata('errorreason', 'register');
        };
      };
      redirect($_SERVER['HTTP_REFERER'], 'refresh');
    }

    public function updatecomments() {
      $result = array(
        'comments' => $this->recipe_model->get_posts(),
        'currentuser' => isset($_SESSION['currentuser']) ? $_SESSION['currentuser'] : '',
        'currenturl' => $_SESSION['currenturl']
      );

      $find = '/tastymvc/';
      $result['currenturl'] = str_replace($find, "", $result['currenturl']);
      $result['currenturl'] = $result['currenturl'] . '.php';
      echo json_encode($result, JSON_UNESCAPED_SLASHES);
    }
  }
?>
