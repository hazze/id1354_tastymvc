<?php
/*Start comment_helper.php file */
function print_comment($comments, $title){
  foreach ($comments as $comment) :
    if ($comment['url'] == lcfirst($title).'.php') : ?>
      <div class='comment_container'>
        <h3><?php echo $comment['title']; ?></h3>
        <p class="meta">Posted on: <?php echo $comment['date']; ?> by <span><?php echo $comment['name']; ?></span></p>

        <?php if ($comment['name'] == isset($_SESSION["currentuser"]) ? $_SESSION["currentuser"] : '') : ?>
          <?php
              $attributes = array('class' => 'ajax delete_comment');
              echo form_open('pages/delete', $attributes);
          ?>
            <input type='submit'
                   class='button deleteC'
                   name='deleted_comment'
                   value=<?php echo $comment['id']; ?>
            />
          </form>
        <?php endif; ?>
        <p class='commenttext'><?php echo $comment['commenttext']; ?></p>
        <br />
      </div>
    <?php endif; ?>
  <?php endforeach;
}
/*     * End comment_helper.php file     */
?>
