      <div id="mainHeader">
        <div class="wrap">
          <img src="<?php echo base_url();?>/resources/img/tasty_logo.png" alt="Tasty Recipes Logo" />
          <br class="clear" />
          <a href="calendar" class="button">Recipe Calendar</a>
        </div>
        <!-- end mainHeader wrap -->
      </div>
      <!-- end mainHeader -->
    </div>
    <!-- end header -->

    <div id="recipes">
      <div class="wrap">
        <ul>
          <li class="container">
              <img src="<?php echo base_url();?>/resources/img/pancakes.png" alt="Pancakes" />
              <div class="overlay">
                <div class="text">
                  <h3>Pancakes</h3>
                  <a href="<?php echo base_url();?>pancakes">Recipe</a>
                </div>
              </div>
          </li>
          <li class="container">
              <img src="<?php echo base_url();?>/resources/img/meatballs.png" alt="Swedish meatballs" />
              <div class="overlay">
                <div class="text">
                  <h3>Meatballs</h3>
                  <a href="<?php echo base_url();?>meatballs">Recipe</a>
                </div>
              </div>
          </li>
          <li>
            <a href="#">
              <img src="<?php echo base_url();?>/resources/img/placeholder.png" alt="Brows more recipes" />
            </a>
          </li>
          <li>
            <a href="#">
              <img src="<?php echo base_url();?>/resources/img/placeholder.png" alt="Placeholder" />
            </a>
          </li>
          <li>
            <a href="#">
              <img src="<?php echo base_url();?>/resources/img/placeholder.png" alt="Placeholder" />
            </a>
          </li>
          <li>
            <a href="#">
              <img src="<?php echo base_url();?>/resources/img/placeholder.png" alt="Placeholder" />
            </a>
          </li>
          <li>
            <a href="#">
              <img src="<?php echo base_url();?>/resources/img/placeholder.png" alt="Placeholder" />
            </a>
          </li>
          <li>
            <a href="#">
              <img src="<?php echo base_url();?>/resources/img/placeholder.png" alt="Placeholder" />
            </a>
          </li>
          <li>
            <a href="#">
              <img src="<?php echo base_url();?>/resources/img/placeholder.png" alt="Placeholder" />
            </a>
          </li>
        </ul>
      </div>
      <!-- end recipes wrap -->
    </div>
    <!-- end recipes -->
